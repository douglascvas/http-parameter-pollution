package app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request, HttpServletResponse response) {
        try {
            Action action = Action.fromString(request.getParameter("action"));
            Amount amount = new Amount(request.getParameter("amount"));

            if (action.equals(Action.TRANSFER)) {
                System.out.println("Verify Controller: Going to transfer $" + amount);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                ResponseEntity<String> responseEntity = restTemplate.getForEntity(fakePaymentUrl + "?action=" + action.toString().toLowerCase() + "&amount=" + amount.getValue(), String.class);
                return responseEntity.getBody();
            } else if (action.equals(Action.WITHDRAW)) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch (InvalidValueException e) {
            response.setStatus(400);
            return e.getMessage();
        }
    }

}
