package app;

public enum Action {
    TRANSFER,
    WITHDRAW;

    public static Action fromString(String value) {
        return Action.valueOf(value.toUpperCase());
    }
}
