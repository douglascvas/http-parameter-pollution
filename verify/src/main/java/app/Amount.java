package app;

import java.math.BigDecimal;

public class Amount {
    private BigDecimal value;

    public Amount(String stringValue) {
        try {
            value = new BigDecimal(stringValue);
        } catch (Exception e) {
            throw new InvalidValueException("Invalid amount");
        }
        if (value.compareTo(BigDecimal.ZERO) <= 0 || value.compareTo(BigDecimal.valueOf(10000d)) > 0)
            throw new RuntimeException("Invalid amount");
    }

    public BigDecimal getValue() {
        return value;
    }
}
