package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Security unit tests")
@WebMvcTest
@Tag("security")
public class appSecuritySpec {

    @Autowired
    private MockMvc mockMvc;

    static {
        // Virtualbox: http://10.0.2.15:8082/
        // Osx: http://host.docker.internal:8082/
        // Other systems: http://(IP address of docker host):8082/
        System.setProperty("paymenturl", "http://10.2.219.36:8082/");
    }

    @Test
    public void should_rejectWithdraw() throws Exception {
        this.mockMvc.perform(post("/").param("action", "transfer").param("amount", "100%26action=withdraw"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("Invalid amount."));
    }
}
